import requests

def get_info():
    response = requests.get(url="https://api.binance.com/api/v3/exchangeInfo")

    with open("response.txt", "w") as file:
        file.write(response.text)

    return response.text

def main():
    print(get_info())

if __name__ == '__main__':
    main()